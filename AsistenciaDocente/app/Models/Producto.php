<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
  protected $primaryKey = 'id_producto';

  protected $table= 'productos';

  protected $fillable = [
    'id_producto',  'nombre_producto', 'precio_producto', 'categorias_id_categoria', 'marcas_id_marca', 'imagenes_id_imagen', 'created_at', 'updated_at'
  ];

  public function marcas(){

    return $this->belongsTo('app\Models\Marca');
  }
  
  public function categorias(){

    return $this->belongsTo('app\Models\Categoria');
  }

  public function producto(){
    return $this->hasMany('app\Models\Imagen');
  }
    
}
