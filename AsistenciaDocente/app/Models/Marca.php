<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
	protected $primaryKey = 'id_marca';

    protected $table= 'marcas';

    protected $fillable = [
    'id_marca',  'nombre_marca', 'created_at', 'updated_at'
    ];

  	public function producto(){

    return $this->hasMany('app\Models\Producto');
  	
  	}
}
