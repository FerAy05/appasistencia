<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $primaryKey = 'id_categoria';

    protected $table= 'categorias';

    protected $fillable = [
    'id_categoria',  'nombre_categoria', 'created_at', 'updated_at'];

  public function productos(){

    return $this->hasMany('app\Models\Producto');
    }
}
