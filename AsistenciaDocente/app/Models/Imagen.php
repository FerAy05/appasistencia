<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Imagen extends Model
{
    protected $primaryKey = 'id_imagen';

    protected $table = 'imagenes';
    
    protected $fillable = [
        'id_imagen', 'nombre_imagen', 'producto_id_producto'
    ];

    public function producto(){
        return $this->belongsTo('app\Models\Producto');
    }
}
