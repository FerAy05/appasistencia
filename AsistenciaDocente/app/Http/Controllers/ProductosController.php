<?php

namespace App\Http\Controllers;
use App\Models\Producto;
use App\Models\Marca;
use App\Models\Categoria;
use App\Models\Imagen;
use Illuminate\Http\Request;
use App\Http\Requests\ProductoRequest;
use Illuminate\Support\Facades\Redirect;

class ProductosController extends Controller
{

    public function index()  //Consulta SQL a la BD
    {
            

        $datos = Producto::select('productos.id_producto','productos.nombre_producto as producto', 'productos.precio_producto', 'marcas.nombre_marca as marca', 'categorias.nombre_categoria as categoria')
            ->join('marcas','marcas.id_marca','=','productos.marcas_id_marca')
            ->join('categorias', 'categorias.id_categoria','=','productos.categorias_id_categoria')
            ->get();

        return view('productos/index')->with('datos', $datos);
    }

    public function create() //Crear un nuevo Registro
    {
        $datosMarca=Marca::all();
        $datosCategoria=Categoria::all();

        return view('productos/create')->with('datosMarca', $datosMarca)->with('datosCategoria', $datosCategoria);
    }


    public function store(ProductoRequest $request) //Guarda el registro en la BD
    {
        


        if($request->hasFile('image')){
            $file = $request->file('image');
            $fileName = time().'.'.$file->getClientOriginalName();
            $path = public_path('/images/productos/');
            $file->move($path,$fileName);
            
                
            $dato = new Producto($request->all());
            $dato -> save();

            $image = new Imagen();
            $image -> nombre_imagen = $fileName;
            $image -> producto()->associate($dato);
            $image -> save();
        }else{
            $dato = new Producto($request->all());
            $dato -> save();
        }


        return redirect()->route('productos.index'); 
    }


    public function show(Producto $producto)
    {
        //
    }

    public function edit($producto)
    {
        $marca = Marca::all();
        $categoria = Categoria::all();
        $imagen = Imagen::all();
        $producto = Producto::findOrFail($producto);
        return view('productos.edit')->with('marca', $marca)->with('producto', $producto)->with('imagen',$imagen)->with('categoria', $categoria);
    }


    public function update(ProductoRequest $request, $producto)
    {
        $this->validate($request, [
            'nombre_producto' => 'required', 
            'precio_producto' => 'required',
        ]);

        $producto = Producto::find($producto);

            $producto->nombre_producto = $request->nombre_producto;
            $producto->precio_producto = $request->precio_producto;
            $producto->marcas_id_marca = $request->marcas_id_marca;
            $producto->categorias_id_categoria = $request->categorias_id_categoria;

        if ($producto->save()){
            return redirect()->route('productos.index')->with('Datos guardados satisfactoriamente');
        }else{  
            return back()->with('El producto no puede ser procesado!, vuelva a intentarlo');
        }
    }


    public function destroy($producto)
    {
        $producto = Producto::findOrFail($producto);
        $producto->delete();
        if($producto->delete()){
            return redirect()->route('productos.index')->with('success', 'El producto se ha eliminado correctamente');
        }else{
            return redirect()->route('productos.index')->with('error', 'El producto no se ha podido eliminar');
        };
        
    }
}
