<?php

namespace App\Http\Controllers;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\Marca;
use Illuminate\Http\Request;
use App\Http\Requests\CategoriaRequest;

class CategoriasController extends Controller
{
   
    public function index()
    {
        $datos= Categoria::all();
        return view('categorias/index')->with('datos', $datos);
    }

    
    public function create()
    {
        $datos = Categoria::all();
        return view('categorias/create')->with('datos', $datos);
    }

    
    public function store(CategoriaRequest $request)
    {
        
        if (Categoria::create($request->all())) {
           return redirect()->route('categorias.index');
        }else{
            return back()->with('El producto no puede ser procesado!, vuelva a intentarlo');
        }
    }

    
    public function show(Categorias $categoria)
    {
        //
    }

    
    public function edit($categoria)
    {
        $categoria = Categoria::findOrFail($categoria);
        return view('categorias.edit')->with('categoria', $categoria);
    }

   
    public function update(CategoriaRequest $request, $categoria)
    {
        $this->validate($request, [
            'nombre_categoria' => 'required', 
        ]);

        $categoria = Categoria::find($categoria);

        $categoria->nombre_categoria = $request->nombre_categoria;

        if ($categoria->save()){
            return redirect()->route('categorias.index')->with('Datos guardados satisfactoriamente');
        }else{  
            return back()->with('El producto no puede ser procesado!, vuelva a intentarlo');
        }
    }

    
    public function destroy($categoria)
    {
        $categoria = Categoria::findOrFail($categoria);
        $categoria->delete();
        if($categoria->delete()){
            return redirect()->route('categorias.index')->with('success', 'La categoria se ha eliminado correctamente');
        }else{
            return redirect()->route('categorias.index')->with('error', 'La categoria no se ha podido eliminar');
        };
    }
}
