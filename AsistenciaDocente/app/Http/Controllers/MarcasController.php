<?php

namespace App\Http\Controllers;
use App\Models\Producto;
use App\Models\Marca;
use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Http\Requests\MarcaRequest;

class MarcasController extends Controller
{
    
    public function index()
    {
        
        $datos= Marca::all();
        return view('marcas/index')->with('datos', $datos);
    }

   
    public function create()
    {
        $datos = Marca::all();
        return view('marcas/create')->with('datos', $datos);
    }

    
    public function store(MarcaRequest $request)
    {
        
        if (Marca::create($request->all())) {
            return redirect()->route('marcas.index');
        }else{
            return back()->with('El producto no puede ser procesado!, vuelva a intentarlo');
        }
    }

    
    public function show(Marca $marca)
    {
        //
    }

    
    public function edit($marca)
    {
        $marca = Marca::findOrFail($marca);
        return view('marcas.edit')->with('marca', $marca);
    }

    
    public function update(MarcaRequest $request, $marca)
    {
        $this->validate($request, [
            'nombre_marca' => 'required', 
        ]);

        $marca = Marca::find($marca);

        $marca->nombre_marca = $request->nombre_marca;

        if ($marca->save()){
            return redirect()->route('marcas.index')->with('Datos guardados satisfactoriamente');
        }else{  
            return back()->with('El producto no puede ser procesado!, vuelva a intentarlo');
        }
    }

    
    public function destroy($marca)
    {
        $marca = Marca::findOrFail($marca);
        $marca->delete();
        
        if($marca->delete()){
            return redirect()->route('marcas.index')->with('success', 'La marca se ha eliminado correctamente');
        }else{
            return redirect()->route('marcas.index')->with('error', 'La marca no se ha podido eliminar');
        };

    }
}
