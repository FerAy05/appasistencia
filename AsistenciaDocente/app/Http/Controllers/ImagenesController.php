<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Imagen;
use App\Models\Producto;
use App\Models\Marca;
use App\Models\Categoria;


class ImagenesController extends Controller
{
    public function index(){
        $images = Imagen::all();
        return view('imagenes.index')->with('images', $images);
    }


}
