<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'nombre_categoria' => 'required|max:255',
        ];
    }

    public function messages(){
        return[
            'nombre_categoria.required' => 'Debe agregar un nombre a la categoria del producto',
        ];
    }
}
