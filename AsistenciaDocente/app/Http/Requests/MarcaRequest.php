<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MarcaRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'nombre_marca' => 'required|max:255',
        ];
    }

    public function messages(){
        return[
            'nombre_marca.required' => 'Debe agregar un nombre a la marca del producto',
        ];
    }
}
