<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductoRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            //'nombre_producto' => 'required|varchar|max:255',
            //'precio_producto' => 'required|numeric|max:55',
            //'descripcion_producto' => 'required|min:20'
            'nombre_producto' => 'required|max:255',
            'precio_producto' => 'required|numeric',
        ];
    }

    public function messages(){
        return[
            'nombre_producto.required' => 'Debe agregar un nombre al producto',
            'precio_producto.required' => 'Debe agregar un precio al producto',
        ];
    }
}
