<!DOCTYPE html>
<html>
<head>
		<title>Sistema Producto</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
</head>
<body>

	<!-- MENU NAVBAR -->

	<nav class="navbar navbar-inverse" style="background-color:#993300;border-color:#993300 ; ">
	  <div class="container-fluid">
	  	
	    <div class="navbar-header">
	      <a class="navbar-brand" href="/" style="color:white;">Sistema de Productos</a>
	    </div>

	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
	      
	      <ul class="nav navbar-nav">
           	<li class=""><a href="/productos" style="color:white;">Productos <span class="sr-only">(actual)</span></a></li>
            <li><a href="/marcas" style="color:white;">Marcas</a></li>
            <li><a href="/categorias" style="color:white;">Categorias</a></li> 
	      </ul>

	      <form class="navbar-form navbar-right" role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-default">Buscar</button>
	      </form>	

	    </div>

	  </div>
	</nav>

		 
  <div class="container">
      @yield('content')
  </div>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="bootstrap.min.js"></script>
</body>
</html>
