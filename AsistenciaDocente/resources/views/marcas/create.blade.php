@extends('layouts.app')
@section('content')

<!--productos.store hace referencia a la función store-->
<div class="row">
<form action="{{ route('marcas.store') }}" method="POST">
	{{ csrf_field() }}
	<fieldset>
		<legend>Nueva Marca</legend>

		<div class="mb-3">
			<label class="form-label">Nombre</label>
			<div>

				@if ($errors->has('nombre_marca'))
					<span style="color:red;">{{$errors->first('nombre_marca')}}</span>
				@endif

				<!--En name y id se pone el nombre de la BD-->
				<input type="text" name="nombre_marca" id="nombre_marca" placeholder="Nombre" class="form-control">	
			</div>
		</div>
		<br>
		<div style="text-align:center;">
			<button type="submit" class="btn btn-primary">Agregar</button>
			<a href="/marcas" class="btn btn-info">Cancelar</a>
		</div>
	</fieldset>
</form>
</div>
@endsection