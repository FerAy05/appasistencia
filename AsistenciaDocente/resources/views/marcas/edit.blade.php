@extends('layouts.app')
@section('content')

<form action="{{ route('marcas.update', $marca->id_marca) }}" method="POST">
	<input type="hidden" name="_method" value="PUT">
		{{ csrf_field() }}

	<fieldset>
		<legend>Editar Marca</legend>

		<div class="mb-3">
			<label class="form-label">Nombre</label>
			<div>

				@if ($errors->has('nombre_marca'))
					<span style="color:red;">{{$errors->first('nombre_marca')}}</span>
				@endif
				
				<!--En name y id se pone el nombre de la BD-->
				<input type="text" name="nombre_marca" id="nombre_marca" value='{{$marca->nombre_marca}}' class="form-control">	
			</div>
		</div>
		<br>
		<div style="text-align:center;">
			<button type="submit" class="btn btn-primary">Actualizar</button>
			<a href="/marcas" class="btn btn-info">Cancelar</a>
		</div>

	</fieldset>
</form>
@endsection