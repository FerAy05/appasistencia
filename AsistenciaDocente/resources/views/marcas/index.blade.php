@extends('layouts.app')
@section('content')

<div class="row">

	<table class="table table-sm">
		
		<div style="text-align:center">
			<h1>Indice de Marcas</h1>
		</div>

		<thead>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Acciones</th>
				<th><a href="/marcas/create" class="btn btn-success">Agregar</a></th>
			</tr>
		</thead>

		<tbody>
			<!--Bucle for-->
			@foreach ($datos as $marca)
			<tr>
				<td>{{$marca->id_marca}}</td>
				<td>{{$marca->nombre_marca}}</td>
				<td><a href="{{route('marcas.edit', ['marca' => $marca->id_marca])}}" class="btn btn-warning">Modificar</a></td>
				<td>
					<form action="{{ route('marcas.destroy', $marca->id_marca) }}" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<button type="submit" class="btn btn-danger" onclick="return confirm('Estas seguro de eliminar la marca? Esto es irreversible.')">Eliminar</button>
					</form>
				</td>
			</tr>
			@endforeach

		</tbody>

	</table>

</div>  
@endsection    

        
     