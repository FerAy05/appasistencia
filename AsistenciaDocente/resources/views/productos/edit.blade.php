@extends('layouts.app')
@section('content')

<form action="{{ route('productos.update', $producto->id_producto) }}" method="POST">
	<input type="hidden" name="_method" value="PUT">
		{{ csrf_field() }}
	
	<fieldset>
		<legend>Editar Producto</legend>

		<div class="mb-3">
			<label class="form-label">Nombre</label>
			<div>

				@if ($errors->has('nombre_producto'))
					<span style="color:red;">{{$errors->first('nombre_producto')}}</span>
				@endif

				<!--En name y id se pone el nombre de la BD-->
				<input type="text" name="nombre_producto" id="nombre_producto" value='{{$producto->nombre_producto}}' class="form-control">	
			</div>
		</div>

		<div class="mb-3">
			<label class="form-label">Precio</label>
			<div>

				@if ($errors->has('precio_producto'))
					<span style="color:red;">{{$errors->first('precio_producto')}}</span>
				@endif
				
				<!--El type aquí será number-->
				<input type="number" name="precio_producto" id="precio_producto" value='{{$producto->precio_producto}}' class="form-control">
			</div>
		</div>

		<div class="mb-3">
			<label class="form-label">Seleccione la marca</label>
			<select class="form-control" id="select" name="marcas_id_marca">
				@foreach ($marca as $marca)
					@if ($marca->id_marca == $producto->marcas_id_marca)
						<option selected value="{{$marca->id_marca}}">{{$marca->nombre_marca}}</option>
					@else
						<option value="{{$marca->id_marca}}">{{$marca->nombre_marca}}</option>
					@endif
				@endforeach
			</select>
		</div>

		<div class="mb-3">
			<label class="form-label">Seleccione la Categoria</label>
			<select class="form-control" id="select" name="categorias_id_categoria">
				@foreach ($categoria as $categoria)
					@if ($categoria->id_categoria == $producto->categorias_id_categoria)
						<option selected value="{{$categoria->id_categoria}}">{{$categoria->nombre_categoria}}</option>
					@else
						<option value="{{$categoria->id_categoria}}">{{$categoria->nombre_categoria}}</option>
					@endif
				@endforeach
			</select>
		</div>
		<br>

		
		<div style="text-align:center;">
			<button type="submit" class="btn btn-primary">Actualizar</button>
			<a href="/productos" class="btn btn-info">Cancelar</a>
		</div>
	</fieldset>
</form>
@endsection