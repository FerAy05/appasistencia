@extends('layouts.app')
@section('content')

<div class="row">
	<table class="table table-sm">

		<div style="text-align:center">
			<h1>Indice de Productos</h1>
		</div>
		
		<thead class="thead-dark">
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Precio</th>
				<th>Marca</th>
				<th>Categoria</th>
				<th>Acciones</th>
				<th><a href="/productos/create" class="btn btn-success">Agregar</a></th>
			</tr>
		</thead>

		<tbody>
			<!--Bucle for-->
			@foreach ($datos as $producto)
			<tr>
				<td>{{$producto->id_producto}}</td>
				<td>{{$producto->producto}}</td>
				<td>{{$producto->precio_producto}}</td>
				<td>{{$producto->marca}}</td>
				<td>{{$producto->categoria}}</td>
				<td><a href="{{route('productos.edit', ['producto' => $producto->id_producto])}}" class="btn btn-warning">Modificar</a></td>
				<td>
					<form action="{{ route('productos.destroy', $producto->id_producto) }}" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<button type="submit" class="btn btn-danger" onclick="return confirm('Estas seguro de eliminar el producto? Esto es irreversible.')">Eliminar</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	
	</table>
</div>      
@endsection
        
     