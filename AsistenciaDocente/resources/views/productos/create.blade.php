@extends('layouts.app')
@section('content')

<!--productos.store hace referencia a la función store-->
<form action="{{ route('productos.store') }}" enctype="multipart/form-data" method="POST">
	{{ csrf_field() }}
	<fieldset>
		<legend>Nuevo Producto</legend>

		<div class="mb-3">
			<label class="form-label">Nombre</label>
			<div>
				
				@if ($errors->has('nombre_producto'))
					<span style="color:red;">{{$errors->first('nombre_producto')}}</span>
				@endif

				<!--En name y id se pone el nombre de la BD-->
				<input type="text" name="nombre_producto" id="nombre_producto" placeholder="Nombre" class="form-control">	
			</div>
		</div>

		<div class="mb-3">
			<label class="form-label">Precio</label>
			<div>

				@if ($errors->has('precio_producto'))
					<span style="color:red;">{{$errors->first('precio_producto')}}</span>
				@endif

				<!--El type aquí será number-->
				<input type="number" name="precio_producto" id="precio_producto" placeholder="Precio" class="form-control">
			</div>
		</div>

	
		<div class="mb-3">
			<label class="form-label">Seleccione la marca</label>
			<div>
				<!--El id en este caso será "select" porque hace referencia a la cajita que tenemos-->
				<!--Aquí se usa el nombre de la tabla Producto-->
				<select id="select" name="marcas_id_marca" class="form-control">
					@foreach ($datosMarca as $marca)
						<!--Aquí se usan los nombres de la tabla Marca-->
						<option value="{{$marca->id_marca}}">{{$marca->nombre_marca}}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="mb-3">
			<label class="form-label">Seleccione la categoria</label>
			<div>
				<!--El id en este caso será "select" porque hace referencia a la cajita que tenemos-->
				<!--Aquí se usa el nombre de la tabla Producto-->
				<select id="select" name="categorias_id_categoria" class="form-control">
					@foreach ($datosCategoria as $categoria)
						<!--Aquí se usan los nombres de la tabla Marca-->
						<option value="{{$categoria->id_categoria}}">{{$categoria->nombre_categoria}}</option>
					@endforeach
				</select>
			</div>
		</div>
		

		<div>
			<label for="photo">Agregar Imagen</label>
			<div class="input-group">
				<input type="file" class="form-control" name="image" aria-describedby="inputGroupFileAddon04" arial-label="Upload">
			</div>
		</div>

		<br>

		<div style="text-align:center;">
			<button type="submit" class="btn btn-success">Guardar</button>
			<a href="/productos" class="btn btn-info">Cancelar</a>
		</div>

	</fieldset>
</form>
@endsection