@extends('layouts.app')
@section('content')

<div class="row">

		<table class="table table-sm" >

		<div style="text-align:center">
			<h1>Indice de Categorias</h1>
		</div>

		<thead class="thead-dark">
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Acciones</th>
				<th><a href="/categorias/create" class="btn btn-success">Agregar</a></th>
			</tr>
		</thead>

		<tbody>
			<!--Bucle for-->
			@foreach ($datos as $categoria)
			<tr>
				<td>{{$categoria->id_categoria}}</td>
				<td>{{$categoria->nombre_categoria}}</td>
				<td><a href="{{route('categorias.edit', ['categoria' => $categoria->id_categoria])}}" class="btn btn-warning">Modificar</a></td>
				<td>
					<form action="{{ route('categorias.destroy', $categoria->id_categoria) }}" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<button type="submit" class="btn btn-danger" onclick="return confirm('Estas seguro de eliminar la categoria? Esto es irreversible.')">Eliminar</button>
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	
	</table>
</div>      
@endsection    

        
     