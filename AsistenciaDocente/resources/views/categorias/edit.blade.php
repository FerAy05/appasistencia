@extends('layouts.app')
@section('content')

<form action="{{ route('categorias.update', $categoria->id_categoria) }}" method="POST">
	<input type="hidden" name="_method" value="PUT">
		{{ csrf_field() }}
	
	<fieldset>
		<legend>Editar Categoria</legend>

		<div class="mb-3">
			<label class="form-label">Nombre</label>
			<div>

				@if ($errors->has('nombre_categoria'))
					<span style="color:red;">{{$errors->first('nombre_categoria')}}</span>
				@endif

				<!--En name y id se pone el nombre de la BD-->
				<input type="text" name="nombre_categoria" id="nombre_categoria" value='{{$categoria->nombre_categoria}}' class="form-control">	
			</div>
		</div>
		<br>
		<div style="text-align:center;">
			<button type="submit" class="btn btn-primary">Actualizar</button>
			<a href="/categorias" class="btn btn-info">Cancelar</a>
		</div>

	</fieldset>
</form>
@endsection    
