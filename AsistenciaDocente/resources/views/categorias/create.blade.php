@extends('layouts.app')
@section('content')

<!--productos.store hace referencia a la función store-->
<form action="{{ route('categorias.store') }}" method="POST">
	{{ csrf_field() }}
	<fieldset>
		<legend>Nueva Categoria</legend>

		<div class="mb-3">
			<label class="form-label">Nombre</label>
			<div>

				@if ($errors->has('nombre_categoria'))
					<span style="color:red;">{{$errors->first('nombre_categoria')}}</span>
				@endif

				<!--En name y id se pone el nombre de la BD-->
				<input type="text" name="nombre_categoria" id="nombre_categoria" placeholder="Nombre" class="form-control">	
			</div>
		</div>
		<br>
		<div style="text-align:center;">
			<button type="submit" class="btn btn-primary">Agregar</button>
			<a href="/categorias" class="btn btn-info">Cancelar</a>
		</div>

	</fieldset>
</form>
@endsection    
