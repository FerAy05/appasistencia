@extends('layouts.app')
@section('content')

<div class="row">
	<legend>Imágenes de Productos</legend>
	@foreach($images as $image)
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-body">
					<img src="/images/productos/{{$image->nombre_imagen}}" class="img-responsive">
				</div>
				<div class="panel-footer"></div>
			</div>
		</div>
	@endforeach
</div>
@endsection